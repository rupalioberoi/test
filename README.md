#API INTERFACE


# Graffiti Removal request

```
GET   https://grafitti311.herokuapp.com/index.json/?last_name=jr.&month=1&year=2015
```

## Get Parameters

Name        | Description                               | Required/Optional
----------- | ----------------------------------------- | -----------------
last_name   |  last_name                                | Required         |
month       |  month                                    | Required         |  
year        |  year                                     | Required         |




## Success Response

```json
{
    "last_name": "jr.",
    "month": "1",
    "year": "2015",
    "controller": "index",
    "action": "index",
    "format": "json"
}
```


# WEB INTERFACE
URL  https://grafitti311.herokuapp.com/?last_name=jr.&month=1&year=2015


Dropbox https://www.dropbox.com/s/jm3gd8utodrycg6/graffiti.png?dl=0

